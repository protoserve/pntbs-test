//
// Created by husky on 7/17/22.
//

#include "helpers.h"

int init_random_subsystem(struct random_subsystem *subsys, uint32_t additional_seed) {
    // we need to get 64 bytes of random data,
    // so as gen_seed() returns a 64-bit number,
    // we need to call it 64 / 8 times
    unsigned char data[64];
    int i, res;
    for (i = 0; i < 64 / 8; i++) {
        uint64_t seed = gen_seed();
        memcpy(data + i * 8, &seed, 8);
    }
    // for extra randomness, take the data and xor it with the additional seed
    for (i = 0; i < 64; i++) {
        data[i] ^= additional_seed;
    }

    // call refill_random_subsystem
    res = refill_random_subsystem(subsys, data);
    memset(data, 0, 64); // clear data
    return res;
}

int refill_random_subsystem(struct random_subsystem *subsys, unsigned char* data) {
    // nonce will be 1
    uint32_t nonce = 1;
    size_t i = 0;
    // temporary buffer to store each set of 64 bytes of data
    unsigned char temp_input[64];
    unsigned char temp_output[64];
    unsigned char key[32];
    struct chacha_ctx ctx;

    memcpy(temp_input, data, 64);
    // generate a key from the data
    for (i = 0; i < 32; i++) {
        key[i] = data[i];
    }
    chacha_keysetup(&ctx, key, 256);
    i = 0;
    while (i < 2) {
        chacha_ivsetup(&ctx, (const unsigned char *) &nonce, NULL);
        chacha_encrypt_bytes(&ctx, temp_input, temp_output, 64);
        nonce++;
        memcpy(subsys->rand_buffer + i * 64, temp_output, 64);
        memcpy(temp_input, temp_output, 64);
        i++;
    }
    // set the position to 0
    subsys->rand_buffer_pos = 0;
    // make sure to overwrite the key and data, to prevent leaks
    memset(key, 0, 32);

    return 0;
}

int gen_random_bytes(unsigned char *buf, size_t len, struct random_subsystem *subsys) {
// if the buffer is not big enough, return -1
    if (len > 127) {
        return -1;
    }
    // if the buffer is empty, refill it
    if ((subsys->rand_buffer_pos + len) >= 126) {
        refill_random_subsystem(subsys, (unsigned char *) &subsys->rand_buffer[127]);
    }
    // copy the data to the buffer
    memcpy(buf, subsys->rand_buffer + subsys->rand_buffer_pos, len);
    // increment the position
    subsys->rand_buffer_pos += len;
    // overwrite the data, to prevent leaks
    memset(subsys->rand_buffer + subsys->rand_buffer_pos - len, 0, len);
    return 0;
}

uint32_t get_rtc_time() {
    uint16_t clock_lo = 0;
    uint16_t clock_hi = 0;

    _asm {
        mov ah, 0
        int 1ah
        mov clock_lo, dx
        mov clock_hi, cx
    };

    return (clock_hi << 16) | clock_lo;
}

uint32_t read_all_io() {
    uint16_t return_lo, return_hi = 0;
    size_t i;
    for (i = 0; i < 65535; i++) {
        return_lo ^= inb(i);
    }
    for (i = 0; i < 65535; i++) {
        return_hi ^= inb(i);
    }
    return (return_hi << 16) | return_lo;
}

uint64_t gen_seed() {
    uint32_t time_hi, time_lo, io_hi, io_lo;
    time_hi = get_rtc_time();
    time_lo = read_all_io(); // in the future, this should be a different source of entropy
    io_hi = read_all_io();
    io_lo = read_all_io(); // in the future, this should be a different source of entropy
    return (uint64_t) time_hi << 32 | time_lo ^ io_hi ^ io_lo;
}
