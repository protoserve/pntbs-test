#include <stdio.h>
#include "helpers.h"
#include "small-hkdf/sha256.h"
#include "small-hkdf/hmac.h"
#include "small-hkdf/hkdf.h"
#include "libeddsa/lib/eddsa.h"

// because msdos is weird, we need to convert the times that the rtc gives us
// the function will return the duration in milliseconds
uint32_t calculate_time_duration(uint32_t beginning, uint32_t end) {
    // first off, multiply by 55 to get milliseconds
    beginning *= 55;
    end *= 55;
    // now, we need to calculate the difference between the two times
    // this is funny because the rtc will reset to 0 when midnight is reached,
    // thus end may be lower than beginning
    if (end < beginning) {
        // if end is lower than beginning, we need to add the 24 hours to end
        end += 86400000;
    }
    // now, we can calculate the difference
    return end - beginning;
}

void test_random(uint32_t seed) {
    uint32_t last_time, current_time;
    int i;
    struct random_subsystem subsys;
    unsigned char buf[32];
    last_time = get_rtc_time();
    printf("initialising random subsystem...\n");
    init_random_subsystem(&subsys, seed);
    printf("generating random bytes...\n");
    memset(buf, 0, 64);
    gen_random_bytes(buf, 64, &subsys);
    current_time = get_rtc_time();
    printf("random bytes: ");
    for (i = 0; i < 32; i++) {
        printf("%02x", buf[i]);
    }
    printf("\n");

    // fix for the weird msdos time system
    printf("took %d ms to initialise random subsystem and generate 32 bytes\n", calculate_time_duration(last_time, current_time));
    exit(0); // exit here because returning causes some glitches
}

void test_chacha20(uint32_t seed) {
    uint32_t last_time, current_time, key_time, nonce_time, encrypt_time, decrypt_time, total_time;
    const unsigned char one[4] = { 1, 0, 0, 0 };
    struct random_subsystem subsys;
    unsigned char key[32];
    unsigned char nonce[8];
    unsigned char message[20] = "hello world sex 123";
    unsigned char message_encrypt[20], message_decrypt[20], encrypted_data[20];
    struct chacha_ctx ctx_a;
    struct chacha_ctx ctx_b;
    printf("initialising random subsystem...\n");
    init_random_subsystem(&subsys, seed);
    last_time = get_rtc_time();

    printf("generating key...\n");
    gen_random_bytes(key, 32, &subsys);

    current_time = get_rtc_time();
    key_time = calculate_time_duration(last_time, current_time);

    printf("generating nonce...\n");
    gen_random_bytes(nonce, 8, &subsys);

    current_time = get_rtc_time();
    nonce_time = calculate_time_duration(last_time, current_time);

    printf("encrypting message...\n");
    chacha_keysetup(&ctx_a, key, 256);
    chacha_ivsetup(&ctx_a, nonce, one);
    memcpy(message_encrypt, message, 20);
    chacha_encrypt_bytes(&ctx_a, message_encrypt, encrypted_data, 20);

    current_time = get_rtc_time();
    encrypt_time = calculate_time_duration(last_time, current_time);

    printf("decrypting message...\n");
    chacha_keysetup(&ctx_b, key, 256);
    chacha_ivsetup(&ctx_b, nonce, one);
    chacha_encrypt_bytes(&ctx_b, encrypted_data, message_decrypt, 20);

    current_time = get_rtc_time();
    decrypt_time = calculate_time_duration(last_time, current_time);

    current_time = get_rtc_time();
    total_time = calculate_time_duration(last_time, current_time);

    printf("key time: %d ms\n", key_time);
    printf("nonce time: %d ms\n", nonce_time);
    printf("encrypt time: %d ms\n", encrypt_time);
    printf("decrypt time: %d ms\n", decrypt_time);
    printf("message: %s\n", message);
    printf("decrypted message: %s\n", message_decrypt);
    printf("total time: %d ms\n", total_time);
    exit(0); // exit here because returning causes some glitches
}

void test_x25519(uint32_t seed) {
    uint32_t last_time, current_time, total_time;
    struct random_subsystem subsys;
    unsigned char seed25519[32];
    unsigned char public_key_a[32];
    unsigned char private_key_a[32];
    unsigned char public_key_b[32];
    unsigned char private_key_b[32];
    unsigned char ed_public_key[32];
    unsigned char ed_private_key[64];
    unsigned char shared_key_a[32];
    unsigned char shared_key_b[32];
    unsigned char signature_test[64];
    unsigned char *message = "hello world sex 234!";
    size_t i;

    printf("initialising random subsystem...\n");
    init_random_subsystem(&subsys, seed);
    last_time = get_rtc_time();

    printf("generating ed25519 keys...\n");
    gen_random_bytes(seed25519, 32, &subsys);
    ed25519_genpub(public_key_a, seed25519);
    sk_ed25519_to_x25519(private_key_a, seed25519);
    memcpy(seed25519, public_key_a, 32);
    pk_ed25519_to_x25519(public_key_b, seed25519);
    printf("generated key a\n");
    printf("public key a: ");
    for (i = 0; i < 32; i++) {
        printf("%02x", public_key_a[i]);
    }
    printf("\n");
    gen_random_bytes(seed25519, 32, &subsys);
    ed25519_genpub(public_key_b, seed25519);
    sk_ed25519_to_x25519(private_key_b, seed25519);
    printf("generated key b\n");
    printf("public key b: ");
    for (i = 0; i < 32; i++) {
        printf("%02x", public_key_b[i]);
    }
    printf("\n");
    compact_x25519_shared(shared_key_a, private_key_a, public_key_b);
    printf("shared key a: ");
    for (i = 0; i < 32; i++) {
        printf("%02x", shared_key_a[i]);
    }
    printf("\n");
    compact_x25519_shared(shared_key_b, private_key_b, public_key_a);
    printf("shared key b: ");
    for (i = 0; i < 32; i++) {
        printf("%02x", shared_key_b[i]);
    }
    printf("\n");
    printf("generating ed25519 key...\n");
    gen_random_bytes(seed25519, 32, &subsys);
    compact_ed25519_keygen(ed_private_key, ed_public_key, seed25519);
    printf("generated ed25519 key\n");
    printf("ed25519 public key: ");
    for (i = 0; i < 32; i++) {
        printf("%02x", ed_public_key[i]);
    }
    printf("\n");
    printf("signing message...\n");
    compact_ed25519_sign(signature_test, ed_private_key, message, strlen(message));
    printf("signed message\n");
    printf("signature: ");
    for (i = 0; i < 64; i++) {
        printf("%02x", signature_test[i]);
    }
    printf("\n");
    printf("verifying signature...\n");
    if (compact_ed25519_verify(signature_test, ed_public_key, message, strlen(message))) {
        printf("signature verified\n");
    } else {
        printf("signature verification failed\n");
    }
    current_time = get_rtc_time();
    total_time = calculate_time_duration(last_time, current_time);
    printf("total time: %d ms\n", total_time);
    exit(0); // exit here because returning causes some glitches
}

void test_sha256() {
    uint32_t last_time, current_time, total_time;
    unsigned char *message = "hello world";
    unsigned char digest[32];
    size_t i;
    last_time = get_rtc_time();
    printf("hashing...\n");
    hash(message, strlen(message), digest);
    current_time = get_rtc_time();
    total_time = calculate_time_duration(last_time, current_time);
    printf("done\n");
    printf("message: %s\n", message);
    printf("digest: ");
    for (i = 0; i < 32; i++) {
        printf("%02x", digest[i]);
    }
    printf("\n");
    printf("total time: %d ms\n", total_time);
    exit(0); // exit here because returning causes some glitches
}

void test_hmac() {
    uint32_t last_time, current_time, total_time;
    unsigned char *message = "hello world!";
    unsigned char *key = "sex";
    unsigned char *output;
    size_t i;
    output = malloc(HASH_SIZE);
    last_time = get_rtc_time();
    printf("beginning...\n");
    hmac(key, strlen(key), message, strlen(message), output);
    current_time = get_rtc_time();
    total_time = calculate_time_duration(last_time, current_time);
    printf("done\n");
    printf("message: %s\n", message);
    printf("key: %s\n", key);
    printf("output: ");
    for (i = 0; i < HASH_SIZE; i++) {
        printf("%02x", output[i]);
    }
    printf("\n");
    printf("total time: %d ms\n", total_time);
    exit(0); // exit here because returning causes some glitches
}

void test_hkdf() {
    uint32_t last_time, current_time, total_time;
    unsigned char *first_key = "sex";
    unsigned char *info = "sex";
    unsigned char *salt = "sex";
    unsigned char *second_key, *output;
    size_t first_key_len, salt_len, info_len, i;

    first_key_len = strlen(first_key);
    salt_len = strlen(salt);
    info_len = strlen(info);

    second_key = malloc(HASH_SIZE);
    output = malloc(HASH_SIZE);

    last_time = get_rtc_time();

    hkdf_extract(salt, salt_len, first_key, first_key_len, second_key);
    hkdf_expand(second_key, HASH_SIZE, info, info_len, HASH_SIZE, output);

    current_time = get_rtc_time();
    total_time = calculate_time_duration(last_time, current_time);
    printf("done\n");
    printf("first key: %s\n", first_key);
    printf("salt: %s\n", salt);
    printf("info: %s\n", info);
    printf("second key: ");
    for (i = 0; i < HASH_SIZE; i++) {
        printf("%02x", second_key[i]);
    }
    printf("\n");
    printf("output: ");
    for (i = 0; i < HASH_SIZE; i++) {
        printf("%02x", output[i]);
    }
    printf("\n");
    printf("total time: %d ms\n", total_time);
    exit(0); // exit here because returning causes some glitches
}

int main()
{
    // setup variables
    uint32_t entropy, time, seed, current_time, last_time, time_difference;
    int choice = 0;
    // get current time
    last_time = get_rtc_time();
    // print registers
    entropy = read_all_io();
    printf("entropy: %u\n", entropy);
    time = get_rtc_time();
    printf("time: %u\n", time);
    seed = time ^ entropy;
    printf("seed: %u\n", seed);

    // calculate time difference
    current_time = get_rtc_time();
    time_difference = calculate_time_duration(last_time, current_time);
    printf("took %u ms to calculate seed\n", time_difference);

    // print a couple newlines
    printf("\n\n\n\n\n");

    printf("\nplease choose one of the following options:\n");
    printf("1. test chacha20\n");
    printf("2. test x25519 key exchange\n");
    printf("3. test random generation\n");
    printf("4. test sha256\n");
    printf("5. test hmac\n");
    printf("6. test hkdf\n");
    printf("7. benchmark realistic environment\n");
    printf("8. exit\n");

    printf("\nenter your choice: ");
    scanf("%d", &choice);
    switch (choice) {
        case 1:
            test_chacha20(seed);
            break;
        case 2:
            test_x25519(seed);
            break;
        case 3:
            test_random(seed);
            break;
        case 4:
            test_sha256();
            break;
        case 5:
            test_hmac();
            break;
        case 6:
            test_hkdf();
            break;
        case 8:
            exit(0);
        default:
            printf("\nInvalid choice.\n");
            break;
    }
    return 0;
}
