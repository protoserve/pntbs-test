#!/bin/bash
# replace me with the path to your openwatcomv2 tools!
export WATCOM=/run/media/husky/epic_disk/OSS/rel
export PATH=$WATCOM/binl64:$WATCOM/binl:$PATH
export EDPATH=$WATCOM/eddat
export INCLUDE=$WATCOM/lh

# checks if ./build/ exists
if [ ! -d "./build" ]; then
    mkdir build
fi

# checks if ./build/ is empty, and if not, deletes it
if [ -d "./build" ]; then
    if [ "$(ls -A ./build)" ]; then
        rm -rf ./build/*
    fi
fi

cd ./build || exit

# cmake watcom command as specified by https://github.com/open-watcom/open-watcom-v2/wiki/OW-tools-usage-with-CMake
cmake .. -G "Watcom WMake" -D CMAKE_SYSTEM_NAME=DOS -D CMAKE_SYSTEM_PROCESSOR=I86 -DCMAKE_BUILD_TYPE=Release
wmake