//
// Created by husky on 7/17/22.
//

#ifndef PNTBSTST_HELPERS_H
#define PNTBSTST_HELPERS_H

#include "stdint.h"
#include "stddef.h"
#include "memory.h"
#include "chachapoly/chacha.h"

uint16_t inb(uint16_t port);
#pragma aux inb = \
    "in ax, dx" \
    parm [dx] \
    value [ax] \
    modify [ax] [dx];

struct random_subsystem {
    unsigned char rand_buffer[128];
    size_t rand_buffer_pos;
};

int init_random_subsystem(struct random_subsystem *subsys, uint32_t additional_seed);

int refill_random_subsystem(struct random_subsystem *subsys, unsigned char* data);

int gen_random_bytes(unsigned char *buf, size_t len, struct random_subsystem *subsys);

uint64_t gen_seed();

// returns data from rtc clock
uint32_t get_rtc_time();

// reads every possible io port and returns a xor of all the values
uint32_t read_all_io();

// wrapper for assembly function _inb
uint16_t inb(uint16_t port);

#endif //PNTBSTST_HELPERS_H
